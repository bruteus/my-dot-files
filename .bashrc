# bruteus .bashrc
# ---------------

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Aliases
alias ls='ls --color=auto'
alias l='ls'
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias cx='clear'

# Color definitions for bash prompt
USERNAME_COLOR='\[\033[38;5;33m\]'
HOSTNAME_COLOR='\[\033[38;5;99m\]'
DIRECTORY_COLOR='\[\033[38;5;161m\]'
SPECIALCHAR_COLOR='\[\033[38;5;40m\]'
RESET='\[$(tput sgr0)\]'

PROMPT_CHAR="\\$"

# Change some values if USER is root
if [ $USER = "root" ]; then
	PROMPT_CHAR="#"
	USERNAME_COLOR='[\033[38;5;196m\]'
fi

export PS1="${SPECIALCHAR_COLOR}[ ${USERNAME_COLOR}\u ${HOSTNAME_COLOR}\h ${SPECIALCHAR_COLOR}] ${DIRECTORY_COLOR}\W ${SPECIALCHAR_COLOR}${PROMPT_CHAR} ${RESET}"

# Enable bash completion
[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

# Add my scripts to path
PATH=$PATH:~/Documents/scripts
